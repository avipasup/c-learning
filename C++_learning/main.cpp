//
//  main.cpp
//  c++ learning
//
//  Created by Avinash Pasupulate on 28/02/16.
//  Copyright © 2016 Avinash Pasupulate. All rights reserved.
// my first program

#include <iostream>
#include <vector>
#include <string>
#include <fstream>

using namespace std; //use this or must use 'std::' before every cout

int main(){
    cout << "Hello World!" << endl;
    
//data types
    
    const double pi = 3.14159265;
    char mygrade = 'A';
    bool ishappy = true;
    int myage = 26;
    float favnum= 3.162812;
    double otherfav = 1.8667544581212;

    int largestint = 2147483647;
    
    cout << "Favorite number : " << favnum << endl;
    cout << "pi value : " << pi << endl;
    cout << "my Age : " << myage << endl;
    cout << "Am I happy : " << ishappy << endl;
    cout << "my grade is : " << mygrade << endl;
    cout << "my other favorite number is : " << otherfav <<endl;
    
    cout << "the largest int is : " << largestint << endl;
    cout << "Size of int is : " << sizeof(myage) << endl;
 
//Increments and decrements

    int i=5;
    cout << "i++ : " << i++ << endl;
    cout << "++i : " << ++i << endl;
    cout << "i-- : " << i-- << endl;
    cout << "--i : " << --i << endl;
    
    cout << "1+2-3*4/2 : " << 1+2-3*4/2 << endl;
    cout << "(1+2-3)*4/2 : " << (1+2-3)*4/2 << endl;
    
    cout << "4/5 is : " << 4/5 << endl;
    cout << "4/5 is : " << (float) 4/5 << endl;
 
 // if...else statement
    
    int age = 70;
    int ageatlastexam = 16;
    bool isnotdrunk = true;
    
    if ((age >= 1) && (age < 16)){
        cout << "You can't drive" << endl;
    }else if(! isnotdrunk){
        cout << "You can't drive" << endl;
    } else if (age >= 80 && ((age > 100) || ((age - ageatlastexam)>5))){
        cout << "You can't drive" << endl;
    } else{
        cout << "You can drive" << endl;
    }
    
 //switch
    
    int greetingOption = 3;
    switch (greetingOption) {
        case 1:
            cout << "Hallo" << endl;
            break;
        case 2:
            cout << "Hola" << endl;
            break;
            
        default:
            cout << "Hello" << endl;
    }
    
//array
    int myFavNums[5];
    int badNums[5]= {4,15,12,67,52};
    cout << "Bad Number 1 : " << badNums[0] << endl;
    char myName[7][11]= {{'A','V','I', 'N','A', 'S', 'H'},
        {'_','P','A','S','U','P','U','L','A','T','E'}};
    
    cout << "The 2nd leter in the 2nd array is : " << myName[1][1];
    
    myName[0][2] = 'e';
    cout << "the changed value at [0][2] is : " << myName[0][2] << endl;

//nested for with array
    
    cout << "The Array is : " << endl;
    for (int i=0; i<7;i++){
        for (int j=0; j<11; j++){
            cout << myName[i][j];
        }
        cout << endl;
    }

//while loop
    
    int randNum= (rand()%100)+1;
    
    while (randNum != 100) {
        cout << randNum << ", ";
        
        randNum = (rand()%100)+1;
    }
    cout << endl;
    int index=1;
    while(index<=10){
        cout << index << endl;
        index++;
    }
//do .. while loop
    
    string numberGuessed;
    int intnumberGuessed=0;
    do {
        cout << "Guess a Number between 1 to 10 : ";
        getline(cin,numberGuessed);
        intnumberGuessed =  stoi(numberGuessed);
        cout << intnumberGuessed << endl;
        
    } while(intnumberGuessed != 4);
    cout << "You win!!" << endl;
    
    return 0;
}